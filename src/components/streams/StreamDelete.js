import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Modal from '../Modal';
import history from '../../history';
import {
    fetchStream,
    deleteStream
} from '../../actions'

class StreamDelete extends Component {
streamId = this.props.match.params.id;
componentDidMount() {
    this.props.fetchStream(this.streamId);
}

deleteStream = () => {
    this.props.deleteStream(this.streamId);
}

renderActions(){
    return(
        <>
            <Link to="/" className="ui button">Cancel</Link>
            <button onClick={this.deleteStream} className="ui button negative">Delete</button>
        </>
    )
}


render(){
    if (!this.props.stream) {
        return <div>Are you sure you want to delete this stream?</div>
    }
    return (
            <Modal 
                title="Delete Stream"
                content = {`Are you sure you want to delete "${this.props.stream.title}"?`}
                actions = {this.renderActions()}
                onDismiss={() => history.push('/')}
                />
    )
}
}

const mapStateToProps = (state, ownProps) => {
    return {stream: state.streams[ownProps.match.params.id] }    
}

export default connect(mapStateToProps, {fetchStream, deleteStream})(StreamDelete);
