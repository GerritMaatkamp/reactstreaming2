import React, {Component} from 'react';
import { connect } from 'react-redux';
import {fetchStream} from '../../actions';

class StreamShow extends Component {
    streamId = this.props.match.params.id;
    componentDidMount() {
        this.props.fetchStream(this.streamId);
    }
    
    render(){        
        if (!this.props.stream) {
            return <div>Loading...</div>
        } else {

            const { title, description} = this.props.stream;
            return (
                <div>
                    <h1 className="header"> {title} </h1>
                    <p>{description}</p>  
                </div>
            ) 
        }
    }
}

const mapStateToProps = (state, ownProps) => {    
    return { stream: state.streams[ownProps.match.params.id]}
}

export default connect(mapStateToProps, {fetchStream})(StreamShow);
