import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, signOut} from '../actions'

class GoogleAuth extends Component {
    authCredentials = {
        clientId: '254187183328-u96hq5em65glgrctusu75glakpnmpffe.apps.googleusercontent.com',
        scope: 'email'
    }

    componentDidMount() {
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init(this.authCredentials).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange);
            });
        });
    }

    onAuthChange = (isSignedIn) => {
        if (isSignedIn) {
            this.props.signIn(this.auth.currentUser.get().getId());
        } else {
            this.props.signOut();
        }
    }

    onSignInClick = () => {
        window.gapi.auth2.getAuthInstance().signIn();
    }

    onSignOutClick  = () => {
        window.gapi.auth2.getAuthInstance().signOut()
    }

    renderAuthButton() {
        if (this.props.isSignedIn === null) {
            return <div><i className="notched circle loading icon"></i></div>;
        } else if (this.props.isSignedIn) {
            return (
                    <button  
                        onClick={this.onSignOutClick}
                        className="ui red google button">
                        <i className="google icon"></i>
                        Sign out
                    </button>
                    )
            } else {
                return (
                    <button 
                        onClick={this.onSignInClick}
                        className="ui green google button">
                        <i className="google icon"></i>
                        Sign in
                    </button>        
                )
        }
    }

    render() {
        return (
            <div className="item">{this.renderAuthButton()}</div>
        )
    }
}

const mapStateToProps = (state) => {
    return { isSignedIn: state.auth.isSignedIn}
}

export default connect(mapStateToProps, {
    signIn,
    signOut
})(GoogleAuth);